<?php
// Setup constants for selects
$form = array();    // info to pass to the form

// setup a unique register id
if (isset($_SESSION['reg_id']) && $_SESSION['reg_id'] != '') {
	$form['reg_id'] = $_SESSION['reg_id'];  // set up uniq id
} else {
	$form['reg_id'] = md5(uniqid(rand(), true));    // set up uniq id
	$_SESSION['reg_id'] = $form['reg_id'];
}

$form['months'] = array(    'Jan' => 1,
							'Feb' => 2,
							'Mar' => 3,
							'Apr' => 4,
							'May' => 5,
							'Jun' => 6,
							'Jul' => 7,
							'Aug' => 8,
							'Sep' => 9,
							'Oct' => 10,
							'Nov' => 11,
							'Dec' => 12
						);
// create years for DOB field, note age range 18 - 118
$form['years'] = array();
$youngest_year = date('Y') - 18;        // year less 18, so at least 18 years old
$oldest_year = $youngest_year - 100;    // youngest_year less 100, so can be as old as 118
for ($i = $youngest_year; $i >= $oldest_year; $i--) {
	$form['years'][] = $i;
}
// grab hear about stuff for form if there is some
$form['hearabout_options'] = false;
$GLOBALS['linkid'] = ($GLOBALS['linkid']) ? $GLOBALS['linkid'] : db_connect($GLOBALS['default_dbname']);
$query = "SELECT text FROM hearabout WHERE siteid='".mysql_real_escape_string($GLOBALS['siteid'])."' ORDER BY text ASC";
$res = @ mysql_query($query);
if ($res && @ mysql_num_rows($res) > 0) {
	while($row = @ mysql_fetch_assoc($res)) {
		$form['hearabout_options'][] = $row['text'];
	}
	@ mysql_free_result($res);
}
?>
		<h1>
			<img src="sysimages/furniture/heart-small.gif" hspace="5" vspace="5" align="absmiddle" alt="heart image" /> Sign Up, and get dating fast</h1>
		<p>
			Sign up now by filling in a short profile below and start searching for your ideal partner within minutes.<br />
		</p><?php if ($errors) { ?>
		<p id="errors">
			Whoops...something needs correcting, the bits in question have been highlighted.
		</p><?php } ?>
		<form action="join.php" method="post" enctype="multipart/form-data">
			<input name="reg_id" type="hidden" value="<?=$form['reg_id']?>" /> <input name="action" type="hidden" value="join_fast" />
			<fieldset>
				<legend>Please fill in your  details below</legend>
		<table width="100%" cellpadding="2" cellspacing="2" border="0">
					<tr>
						<td width="140" valign="top" class="txtright">
							Your e-mail address is:
						</td>
						<td>
							<input id="join_email" name="join_email" type="text" size="30" value="<?=$_POST['join_email']?>" <?= ($errors['join_email']) ? ' class="error"' : ''?> /> <span class="required">*</span> <span class="required">(p)</span> <?=$errors['join_email']?>
							</td>
					</tr>
					<tr>
						<td valign="top" class="txtright">
							Your login password:
						</td>
						<td>
							<input id="join_password" name="join_password" type="password" size="30" value="<?=$_POST['join_password']?>" <?= ($errors['join_password']) ? ' class="error"' : ''?> /> <span class="required">* (p)</span> <?=$errors['join_password']?>
							</td>
					</tr>
					<tr>
						<td valign="top" class="txtright">
							Your first name is:
						</td>
						<td>
							<input id="join_firstname" name="join_firstname" type="text" size="30" value="<?=$_POST['join_firstname']?>" <?= ($errors['join_firstname']) ? ' class="error"' : ''?> /> <span class="required">* (p)</span> <?=$errors['join_firstname']?>
							</td>
					</tr>
					<tr>
						<td valign="top" class="txtright">
							You are :
						</td>
						<td>
							<select name="join_sex" id="join_sex" <?= ($errors['join_sex']) ? ' class="error"' : ''?>>
								<option value="0" selected="selected">
									Select
								</option>
								<option value="1" <?php if ($_POST['join_sex'] == 1) echo 'selected'; ?>>
									Male
								</option>
								<option value="2" <?php if ($_POST['join_sex'] == 2) echo 'selected'; ?>>
									Female
								</option>
							</select> <span class="required">*</span> <?=$errors['join_sex']?>
							</td>
					</tr>
					<tr>
						<td valign="top" class="txtright">
							and born on:						</td>
						<td>
							<select id="join_ddob" name="join_ddob">
								<?php for($i=1;$i<=31;$i++) { ?>
								<option value="<?=$i?>" <?php if ($_POST['join_ddob'] == $i) echo 'selected'; ?>>
									<?=$i?>
									</option><?php } ?>
							</select> <select id="join_mdob" name="join_mdob">
								<?php foreach($form['months'] as $month_name => $month_num) { ?>
								<option value="<?=$month_num?>" <?php if ($_POST['join_mdob'] == $month_num) echo 'selected'; ?>>
									<?=$month_name?>
									</option><?php } ?>
							</select> <?php
															$bottom_age = 18;
															$this_year = date('Y');
															$year_diff = $this_year - $bottom_age;
														?> <select id="join_ydob" name="join_ydob">
								<?php foreach($form['years'] as $y) { ?>
								<option value="<?=$y?>" <?php if ($_POST['join_ydob'] == $y) echo 'selected'; ?>>
									<?=$y?>
									</option><?php } ?>
							</select> <span class="required">*</span> <?=$errors['join_date']?>
							</td>
					</tr>
				    <tr>
				      <td valign="top" class="txtright">Living in:</td>
				      <td><?php counties('Choose One','join_location'); ?>
				        <?=$errors['join_location']?></td>
				    </tr>
				    <tr>
				      <td valign="top" class="txtright">and your 
				          <?php if (!isset($GLOBALS['country']) || $GLOBALS['country'] == "UK") { ?>
				          post code
				          <? } else { ?>
				          zip code
				          <? } ?>
				      is:</td>
				      <td><input name="join_postcode" type="text" id="join_postcode" value="<?=$_POST['join_postcode']?>" size="10"<?= ($errors['join_postcode']) ? ' class="error"' : ''?>>
				        <span class="required">* (p) </span>used for searches
				        <?=$errors['join_postcode']?></td>
				    </tr>
					<?php if (is_array($form['hearabout_options']) && count($form['hearabout_options'])) {?>
					<tr>
						<td valign="top" class="txtright">
							How did you hear of us?
						</td>
						<td>
							<select id="join_hearabout" name="join_hearabout" <?= ($errors['join_hearabout']) ? ' class="error"' : ''?>>
								<option value="unknown">
									Select
								</option><?php foreach($form['hearabout_options'] as $hb) { ?>
								<option value="<?=$hb?>" <?php if ($_POST['join_hearabout'] == $hb) echo 'selected'; ?>>
									<?=$hb?>
									</option><?php } ?>
							</select> <?=$errors['join_hearabout']?>
							</td>
					</tr><?php } ?>
					<tr>
						<td colspan="2" class="txtcenter">
							By clicking &quot;<strong>join free</strong>&quot; you are agreeing to our <a href="terms.php" target="_blank">Terms &amp; Conditions</a>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="txtcenter">
							<input name="submit" type="submit" id="submit" value="click to join free now" />
						</td>
					</tr>
					<tr>
						<td colspan="2" class="required txtcenter">
							* denotes a required field<br />
							(p) = private, never revealed to other members
						</td>
					</tr>
				</table>

			</fieldset>
		</form><?php
				if (!$errors) include_once("_tracking_join_start.php"); // User landed on first join page for the first time (ie. no errors)
				?>
